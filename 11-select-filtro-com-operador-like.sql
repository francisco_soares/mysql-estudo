﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;
/* ========== // ========== */
/*(AULA: 405)*/
/*Usando filtro LIKE*/
-- Permite realizar filtros com base em uma pesquisa de caracteres dentro de uma coluna textual
-- Caracteres curingas
-- [%] - Indica que pode haver a existência de qualquer conjunto de caracter no texto.
-- [_] - Indica que pode haver a existência de um ou mais caracteres em uma posição especifica do texto.
SELECT
  *
FROM
  alunos
WHERE
  nome LIKE 'Evelyn';

-- O LIKE '%e', informa que deve trazer qualquer conjunto de caracte a esquerda onde termine com a letra E
SELECT
  *
FROM
  alunos
WHERE
  nome LIKE '%e';

SELECT
  *
FROM
  alunos
WHERE
  nome LIKE '%ne';


-- O LIKE 'C%' deve trazer qualquer conjunto de caracter a  direita onde comece com a letra C.
SELECT
  *
FROM
  alunos
WHERE
  nome LIKE 'C%';


-- O LIKE '%a%' deve trazer qualquer conjunto de caracter a esquerda e direita onde tenha a letra A dentro.
SELECT
  *
FROM
  alunos
WHERE
  nome LIKE '%a%';


-- O LIKE '_riel' deve trazer um unico caracter que no final termine com RIEL
SELECT
  *
FROM
  alunos
WHERE
  nome LIKE '_riel';


-- O LIKE '_ru_' deve trazer 4 caracteres onde deve ter 1 caracter na começo e fim e no meio deve conter o RU.
SELECT
  *
FROM
  alunos
WHERE
  nome LIKE '_ru_';

-- O LIKE 'I__' deve trazer 3 caracteres onde deve começar com I e terminar com 2 caracteres indefinidos no final.
SELECT
  *
FROM
  alunos
WHERE
  nome LIKE 'I__';


-- O LIKE '%tt_' deve trazer qualquer conjunto de caracter no começo, contendo TT no meio e finaliza com um caracter não definido.
SELECT
  *
FROM
  alunos
WHERE
  nome LIKE '%tt_';
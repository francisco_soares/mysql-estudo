﻿/*(AULA: 421)*/
/*RIGHT JOIN*/
-- EXPLICAÇÃO:
-- Relaciona todos os registro da tabela à direita + CASO EXISTA todos os registro da tabela à esquerda


/* ============================================================= */
/* ===== RELACIONANDO AS TABELAS: tb_clientes e tb_pedidos ===== */
/* ============================================================= */
-- Os resultados serão prioeitário a direita, expecifico a tabela tb_pedidos
SELECT 
    *
FROM
    tb_clientes 
    RIGHT JOIN tb_pedidos ON (tb_clientes.id_cliente = tb_pedidos.id_cliente);
﻿/*(AULA: 423)*/
/*INNER JOIN*/
-- EXPLICAÇÃO:
-- Relaciona todos os registro da tabela à direita e à esquerda onde exista o relacionamento entre registro


/* ============================================================= */
/* ===== RELACIONANDO AS TABELAS COM O LEFT JOIN: tb_pedidos e tb_pedidos_produtos / tb_pedidos_produtos e  tb_produtos ===== */
/* ============================================================= */
-- A relação será feita da seguinte forma: tb_pedidos será ligada ao tb_pedidos_produtos, assim tará os resultados em LEFT, já o segundo será ligado pelo tb_pedidos_produtos com o tb_produtos.
SELECT 
    *
FROM
    tb_pedidos 
    LEFT JOIN tb_pedidos_produtos ON (tb_pedidos.id_pedido = tb_pedidos_produtos.id_pedido)
    LEFT JOIN tb_produtos ON (tb_pedidos_produtos.id_produto = tb_produtos.id_produto);

 
SELECT 
    *
FROM
    tb_pedidos 
    RIGHT JOIN tb_pedidos_produtos ON (tb_pedidos.id_pedido = tb_pedidos_produtos.id_pedido)
    LEFT JOIN tb_produtos ON (tb_pedidos_produtos.id_produto = tb_produtos.id_produto);

SELECT 
    *
FROM
    tb_pedidos 
    RIGHT JOIN tb_pedidos_produtos ON (tb_pedidos.id_pedido = tb_pedidos_produtos.id_pedido)
    RIGHT JOIN tb_produtos ON (tb_pedidos_produtos.id_produto = tb_produtos.id_produto);


/* ===================================================== */
/* ===== RELACIONANDO AS TABELAS COM O INNER JOIN: ===== */
/* ===================================================== */

SELECT 
    *
FROM
    tb_pedidos 
    INNER JOIN tb_pedidos_produtos ON (tb_pedidos.id_pedido = tb_pedidos_produtos.id_pedido);

SELECT 
    *
FROM
    tb_pedidos 
    INNER JOIN tb_pedidos_produtos ON (tb_pedidos.id_pedido = tb_pedidos_produtos.id_pedido)
    INNER JOIN tb_produtos ON (tb_pedidos_produtos.id_produto = tb_produtos.id_produto);

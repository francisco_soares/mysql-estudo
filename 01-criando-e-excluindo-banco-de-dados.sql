﻿/*[AULA 391] - Criando e excluindo banco de dados*/
/*[LINK] https://www.udemy.com/course/web-completo/learn/lecture/11009264#overview*/

/* ========== // ========== */

/* Criando um Banco de Dados (DATABASE)*/
CREATE DATABASE mysql_estudo;

/* ========== // ========== */

/*Remover/Excluir um Bando de dados (DATABASE)*/
-- DROP DATABASE mysql_estudo;
﻿/*(AULA: 421)*/
/*LEFT JOIN*/
-- EXPLICAÇÃO:
-- Relaciona todos os registro da tabela à esquerda + CASO EXISTA todos os registro à direita


/* ============================================================= */
/* ===== RELACIONANDO AS TABELAS: tb_clientes e tb_pedidos ===== */
/* ============================================================= */
SELECT
  *
FROM
  tb_clientes
  LEFT JOIN tb_pedidos ON (tb_clientes.id_cliente = tb_pedidos.id_cliente);


  /* ============================================================= */
  /* ===== RELACIONANDO AS TABELAS: tb_produtos e tb_imagens ===== */
  /* ============================================================= */
  SELECT
    *
  FROM
    tb_produtos
    LEFT JOIN tb_imagens ON (tb_produtos.id_produto = tb_imagens.id_produto);
﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;
/* ========== // ========== */
/*(AULA: 406)*/
/*ORDER BY*/
-- Ordena registro entre ASC e DESC de uma determinada coluna da tabela.
SELECT
  *
FROM
  alunos
WHERE
  idade BETWEEN 18
  AND 59
ORDER BY
  nome ASC, idade DESC, estado ASC
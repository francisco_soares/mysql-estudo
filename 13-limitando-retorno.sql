﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;
/* ========== // ========== */
/*(AULA: 407)*/
/*LIMIT - OFFSET*/
-- Limitando o resultado de registro com o LIMIT e OFFSET
SELECT
  *
FROM
  alunos
LIMIT
  25;

-- [LEMBRETE] - O ORDER BY sempre vem antes do LIMIT
SELECT
  *
FROM
  alunos
ORDER BY
  nome DESC
LIMIT
  5;

SELECT
  *
FROM
  alunos
ORDER BY
  id_aluno DESC
LIMIT
  15;

-- LIMIT E OFFSET
SELECT
  *
FROM
  alunos
LIMIT
  4 OFFSET 0;

SELECT
  *
FROM
  alunos
LIMIT
  4 -- Mostra a quantidade de registro
  OFFSET 4;
--O OFFSET informe a partir de qual local deve mostrar

SELECT
  *
FROM
  alunos
LIMIT
  4 -- Mostra a quantidade de registro
  OFFSET 8; --O OFFSET informe a partir de qual local deve mostrar

SELECT
  *
FROM
  alunos
ORDER BY
  nome DESC
LIMIT
  4, 8; -- [OFFSET], [LIMIT]
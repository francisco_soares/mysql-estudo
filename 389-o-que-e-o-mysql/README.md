# O que é o MySQL?

![O que é o MySQL](img/o-que-e-o-mysql.png)

O **MySQL** é um +**SGBD** - Sistema de Gerenciamento de Banco de Dados<br>
Ele é *RELACIONAL*, *GRATUITO* e usa o a linguagem *SQL* - Structured Query 
Language ou Linguagem de Consulta  Estruturada.
<br><br>

**RELACIONAL**:<br>
Quando é referido tabelas relacional, estamos referindo tabelas que tem registro 
e esse registro estão relacionados entre elas, como por exemplo um ID. <br>

![Tabelas](img/tabelas.png)


﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;


/* ========== // ========== */

/*[ALTER TABLE <nome-da-tabela> ADD COLUMN]
Comando para inserir uma coluna ja na tabela criada
*/
ALTER TABLE cursos ADD COLUMN carga_horaria VARCHAR(5) NULL;

/*[ALTER TABLE <nome-da-tabela> CHANGE]
Alteração do nome de uma coluna e de suas propriedades, como por exemplo o tipo
*/
-- ALTER TABLE cursos CHANGE carga_horaria carga_hora INT(5) NULL;
ALTER TABLE cursos CHANGE carga_hora carga_horaria INT(5);

/*[ALTER TABLE <nome-da-tabela> DROP]
Permite a remoção de uma coluna da tabela
*/
ALTER TABLE cursos DROP carga_horaria;

SELECT * FROM cursos;

﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;
/* ========== // ========== */
/*(AULA: 412)*/
/*UPDATE*/
-- Atualiza registro dentro das tabelas no banco de dados

UPDATE
  alunos
SET
  nome = 'João'
WHERE
  id_aluno = 13;

UPDATE
  alunos
SET
  interesse = 'Saúde'
WHERE
  idade >= 80;

UPDATE
  alunos
SET
  nome = 'José',
  idade = 25,
  email = 'jose@gmail.com'
WHERE
  id_aluno = 18;
  
UPDATE
  alunos
SET
  nome = 'Maria'
WHERE
  idade BETWEEN 18
  AND 25
  AND estado = 'PA';
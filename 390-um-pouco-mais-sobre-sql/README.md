# Mais sobre o SQL:

O SQL é a linguagem útilizada no MySQL, PostgreSQL, SQL Server, Oracle e 
entre outras.<br><br>

## As 5 subcategorias que o SQL é separada.
**DDL** - Data Definition Languagem - Linguagem de Definição de Dados<br>
- Subcategoria que trata da modelagem de dados.<br><br>


**DML** - Data Manipulation Language - Linguagem de Manipulação de Dados<br>
- Subcategoria que possibilita a Inclusão, Alteração e Remoção de registro dentro da estrutura de dados.<br><br>

**DCL** - Data Control Language - Linguagem de Controle de Dados<br>
- Subcategoria que possibilita o gerenciamento de acesso de usuários externos ao SGBD.<br><br>

**DTL** - Data Transaction Language - Linguagem de Transação de Dados<br>
- Subcategoria que possibilita cancelar as transações de dados no SGBD.<br><br>

**DQL** - Data Query Language - Linguagem de Consulta de Dados<br>
- Subcategoria que possobilita a consulta de dados através de cláusulas.<br><br>

### Nesse curso é abordado as seguintes subcategorias: **DDL**, **DML** e **DQL**<br><br>

## PHPMyAdmin
É uma aplicação Web criada em *PHP* que serve para acessar e manipular o *MySQL*. Ele é uma 
interface *SGBD*
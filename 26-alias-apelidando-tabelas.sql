﻿/*(AULA: 424)*/
/*ALIAS*/
-- EXPLICAÇÃO:
-- Alias e uma forma de apelidar uma tabela pela instrunção AS
SELECT
  *
FROM
  tb_produtos
  LEFT JOIN tb_descricoes_tecnicas ON (
    tb_produtos.id_produto = tb_descricoes_tecnicas.id_produto
  );
  /* ================================= */
  /* ===== APELIDANDO AS TABELAS ===== */
  /* ================================= */
SELECT
  *
FROM
  tb_produtos AS P
  LEFT JOIN tb_descricoes_tecnicas AS DT ON (P.id_produto = DT.id_produto);


SELECT
  P.id_produto,
  P.produto,
  P.valor,
  DT.descricao_tecnica
FROM
  tb_produtos AS P
  LEFT JOIN tb_descricoes_tecnicas AS DT ON (P.id_produto = DT.id_produto);


SELECT
  P.id_produto,
  P.produto,
  P.valor,
  DT.descricao_tecnica
FROM
  tb_produtos AS P
  LEFT JOIN tb_descricoes_tecnicas AS DT ON (P.id_produto = DT.id_produto)
WHERE
  P.valor >= 500;

  
SELECT
  P.id_produto,
  P.produto,
  P.valor,
  DT.descricao_tecnica
FROM
  tb_produtos AS P
  LEFT JOIN tb_descricoes_tecnicas AS DT ON (P.id_produto = DT.id_produto)
WHERE
  P.valor >= 500
ORDER BY
  P.valor ASC;
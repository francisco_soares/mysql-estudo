﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;
/* ========== // ========== */
/*(AULA: 402)*/
/*Usando filtro WHERE
*/
/*[OPERADOR LÓGICO AND]*/
-- Todas as condições devem ser verdadeiras
SELECT
  *
FROM
  alunos
WHERE
  interesse = 'Jogos'
  AND idade > 45;
SELECT
  *
FROM
  alunos
WHERE
  interesse = 'Jogos'
  AND idade > 45
  AND estado = 'RN';
  

  /*[OPERADOR LÓGICO OR]*/
  -- Uma das operações precisa ser verdadeira.
SELECT
  *
FROM
  alunos
WHERE
  interesse = 'Jogos'
  OR idade > 45;
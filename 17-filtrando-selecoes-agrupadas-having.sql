﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;
/* ========== // ========== */
/*(AULA: 409)*/
/*HAVING*/
-- Serve para realizadar filtros sobre o resultado dos agrupamentos (GROUP BY)
-- O HAVING não existe sem o GROUP BY
SELECT
  estado,
  COUNT(*) AS total_de_registro_estado
FROM
  alunos
GROUP BY
  estado;


-- Inserindo o HAVING
SELECT
  estado,
  COUNT(*) AS total_de_registro_estado
FROM
  alunos
GROUP BY
  estado
HAVING
  total_de_registro_estado >= 5;


SELECT
  estado,
  COUNT(*) AS total_de_registro_estado
FROM
  alunos
GROUP BY
  estado
HAVING
  estado IN('MG', 'SP');

SELECT
  estado,
  COUNT(*) AS total_de_registro_estado
FROM
  alunos
GROUP BY
  estado
HAVING
  estado IN('CE', 'SC') AND total_de_registro_estado > 4;

-- Usando o WHERE
SELECT
  estado,
  COUNT(*) AS total_de_registro_estado
FROM
  alunos
WHERE
  interesse != 'Esporte'
GROUP BY
  estado
HAVING
  total_de_registro_estado > 3;
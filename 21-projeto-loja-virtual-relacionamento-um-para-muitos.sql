﻿/*(AULA: 417)*/
/*CRIANDO RELACIONAMENTO UM PARA MUITOS*/
-- EXPLICAÇÃO:
-- tb_produtos: Um produto pode ter  vários/MUITOS(AS) Imagens tb_imagens.

/* ======================================== */
/* ===== CRIANDO A TABELA: tb_imagens ===== */
/* ======================================== */
CREATE TABLE tb_imagens(
    id_imagem INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    id_produto INT NOT NULL,
    url_imagem VARCHAR(200) NOT NULL,
    FOREIGN KEY(id_produto) REFERENCES tb_produtos(id_produto)
);

DESCRIBE tb_imagens;

/* ========================================== */
/* ===== INSERINDO REGISTRO: tb_imagens ===== */
/* ========================================== */

INSERT INTO tb_imagens(id_produto, url_imagem) VALUES(1, 'notebook_1.jpg'), (1, 'notebook_2.jpg'), (1, 'notebook_3.jpg');

INSERT INTO tb_imagens(id_produto, url_imagem) VALUES(2, 'smarttv_1.jpg'), (2, 'smarttv_2.jpg');

INSERT INTO tb_imagens(id_produto, url_imagem) VALUES(3, 'smartphone_1.jpg');

SELECT * FROM tb_imagens;


﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;
/* ========== // ========== */
/*(AULA: 408)*/
/*SUM e COUNT*/
-- SUM(<coluna>) - Retorna a SOMA dos valores de todos os registro com base em uma coluna
-- COUNT(*) - Retorna a QUANTIDADE de todos os registro de uma tabela
-- SUM
SELECT
  SUM(investimento)
FROM
  cursos;

SELECT
  SUM(investimento)
FROM
  cursos
WHERE
  ativo = TRUE;


-- COUNT
SELECT
  COUNT(*)
FROM
  cursos
WHERE
  ativo = TRUE;

SELECT
  COUNT(*)
FROM
  cursos
WHERE
  ativo = FALSE;

SELECT
  COUNT(*)
FROM
  cursos;
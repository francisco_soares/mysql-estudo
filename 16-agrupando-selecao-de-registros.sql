﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;
/* ========== // ========== */
/*(AULA: 408)*/
/*GROUP BY*/
-- Agrupa valores com base em uma ou mais colunas cujos valores sejam iguais. Permite realizar funções de agregação em cada subconjunto agrupado de registros.
-- [DICA] - O GROUP BY sempre vem depois do WHERE quando é declarado e antes do ORDER BY e LIMIT

SELECT
  *
FROM
  alunos
GROUP BY interesse


SELECT
  *, COUNT(*)
FROM
  alunos
GROUP BY interesse;

SELECT
  interesse, COUNT(*)
FROM
  alunos
GROUP BY interesse;

SELECT
  interesse, COUNT(*) AS total_por_interesse
FROM
  alunos
GROUP BY interesse;

SELECT
  *
FROM
  alunos
GROUP BY estado;

SELECT
  estado, COUNT(*) AS total_por_estado
FROM
  alunos
GROUP BY estado;

SELECT
  estado, COUNT(*) AS total_por_estado
FROM
  alunos
GROUP BY estado;


﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;
/* ========== // ========== */
/*(AULA: 407)*/
/*MAX, MIN e AVG*/
-- MIN(<coluna>) -  Retorna o MENOR valor de todos os registro com base em uma coluna
-- MAX(<coluna>) - Retorna o MAIOR valor de todos os registros com base em uma coluna
-- AVG(<coluna>) - Retorna a média de todos os registros com base em uma coluna
-- TRUNCADE - Limpa todos os registro dentro de uma tabela do banco.
TRUNCATE cursos;
-- Inserindo dados na tabela cursos
-- INSERT INTO cursos(id_curso, imagem_curso, nome_curso, resumo, data_cadastro, ativo, investimento, carga_horaria) VALUES (1, 'curso_node.jpg', 'Curso Completo do Desenvolvedor NodeJS e MongoDB', 'Resumo do curso de NodeJS', '2018-01-01', 1, 159.99, 15), (2, 'curso_react_native.jpg', 'Multiplataforma Android/IOS com React e Redux', 'Resumo do curso de React Native', '2018-02-01', 1, 204.99, 37), (3, 'angular.jpg', 'Desenvolvimento WEB com ES6, TypeScript e Angular', 'Resumo do curso de ES6, TypeScript e Angular', '2018-03-01', 1, 579.99, 31), (4, 'web_completo_2.jpg', 'Web Completo 2.0', 'Resumo do curso de Web Completo 2.0', '2018-04-01', 1, 579.99, 59), (5, 'linux.jpg', 'Introdução ao GNU/Linux', 'Resumo do curso de GNU/Linux', '2018-05-01', 0, 0, 1)
-- MIN
SELECT
  MIN(investimento)
FROM
  cursos
WHERE
  ativo = 1;
-- MAX
SELECT
  MAX(investimento)
FROM
  cursos
WHERE
  ativo = 1;

-- AVG 
SELECT
  AVG(investimento)
FROM
  cursos
WHERE
  ativo = 1;
﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;
/* ========== // ========== */
/*(AULA: 401)*/
/*Usando filtro WHERE
*/
SELECT
  *
FROM
  alunos
WHERE
  interesse = 'Jogos';

SELECT
  *
FROM
  alunos
WHERE
  idade < 25;

SELECT
  *
FROM
  alunos
WHERE
  idade <= 25;

SELECT
  *
FROM
  alunos
WHERE
  idade > 30;

SELECT
  *
FROM
  alunos
WHERE
  idade >= 35;
﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;
/* ========== // ========== */
/*(AULA: 403)*/
/*Usando filtro BETWEEN*/
-- É utilizado para filtrar valores numéricos ou datas em um intervalo especifico.
SELECT
  *
FROM
  alunos
WHERE
  idade BETWEEN 18
  AND 25;


/* Nessa consulta eu inplementei alguns filtros. Onde a condição está entre ESTADO e IDADE*/
SELECT
  *
FROM
  alunos
WHERE
  (estado BETWEEN 'AL'
  AND 'DF') AND (idade BETWEEN 85 AND 95) ORDER BY estado ASC;
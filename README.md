# Estudo de MySQL aprofundado.

## O que é o MySQL?
O **MySQL** é um +**SGBD** - Sistema de Gerenciamento de Banco de Dados<br>
Ele é *RELACIONAL*, *GRATUITO* e usa o a linguagem *SQL* - Structured Query 
Language ou Linguagem de Consulta  Estruturada.
<br><br>

**RELACIONAL**:<br>
Quando é referido tabelas relacional, estamos referindo tabelas que tem registro 
e esse registro estão relacionados entre elas, como por exemplo um ID. <br>

## Mais sobre o SQL:
O SQL é a linguagem útilizada no MySQL, PostgreSQL, SQL Server, Oracle e 
entre outras.<br><br>

**DDL** - Data Definition Languagem - Linguagem de Definição de Dados<br>
**DML** - Data Manipulation Language - Linguagem de Manipulação de Dados<br>
**DCL** - Data Control Language - Linguagem de Controle de Dados<br>
**DTL** - Data Transaction Language - Linguagem de Transação de Dados<br>
**DQL** - Data Query Language - Linguagem de Consulta de Dados<br>

## PHPMyAdmin
É uma aplicação Web criada em *PHP* que serve para acessar e manipular o *MySQL*. Ele é uma 
interface *SGBD*

## [Aula 392] - Tabelas e tipos de dados - Parte 1 - Um pouco de teoria
[Link aula](https://www.udemy.com/course/web-completo/learn/lecture/11009268#overview)<br><br>


**Tabelas e tipos de dados**:<br><br>

*O que são tabelas?*<br>
Tabelas é semelhante a uma planilha e pode ser entendida como uma unidade de armazenamento.<br>
Uma tabela pode ser constituidas por um número finito de colunas e possui um número indefinido de linhas.<br>
Cada coluna de uma tabela é responsável por um tipo determinado de dados, ex: INT, VARCHAR, CHAR, DATA e etc.<br><br>

**Alguns tipos de campos**<br><br>

*Campos textos*:<br>
- **Text**: (Tamnho variável que armazena uma grande quantidade de caracteres) 
- **Varchar**: (Tamanho variável que armazena de 0 até 255 caracteres)
- **Char**: (Tamanho fixo que armazena de 0 até 255 caracteres)
<br><br>

*Campos numéricos*:<br>
- **Int**: (Valores numéricos inteiros)
- **Float**: (Valores numéricos fracionados)
<br><br>

*Campos de data e hora*:<br>
- **Date**: (Data no formato YYYY/mm/dd)
- **Time**: (Hora)
- **Datetime**: (Combinação de DATE e TIME em um mesmo campo)
<br><br>

## [Aula 394] - Extra - Entendendo a diferença entre tipos de dados CHAR e VARCHAR
[Link aula](https://www.udemy.com/course/web-completo/learn/lecture/11123376#overview)<br><br>

**CHAR**<br>
Se você coloca um CHAR(10) o tamanho ficará de 10 posições no disco.
Sendo assim, se você colocar o nome ISA, ele ocupa 3 posições, porém, em disco vai ser contado as 10 posições que você já definiu no CHAR.<br>

*Vantagem:* - Mais rápido nas pesquisas<br>
*Desvantagens* - Quando mau utilizado, pode reservar espaço em disco de forma desnecessária 
<br><br>

**VARCHAR**
Enquanto o CHAR é de tamanho fixo, o VARCHAR é de tamanho variável.
Sendo assim, se você definir um VARCHAR(10) e você colocar o nome ISA, sé apenas ocupado o tamanho de 3 caracteres.<br>

*Vantagem:* - Por ser de tamanho variável, ocupa apenas o espaço necessário do disco<br>
*Desvantagens* - Por ser de tamanho variável, possui uma meta de dado com uma instrução de finalização do texto, o que produz, em relação ao CHAR maior lentidão em pesquisas.
<br><br>


## [Aula 396] - Incluindo, editando e removendo colunas de uma tabela
[Link aula](https://www.udemy.com/course/web-completo/learn/lecture/11009274#overview)<br><br>

### ALTER TABLE

**ADD**<br>
- Permite a inclusão de uma nova coluna em uma tabela

**CHANGE**<br>
- Permite a alteração do nome de uma coluna e de suas propriedades, como por exemplo o tipo

**DROP**<br>
- Permite a remoção de uma coluna da tabela

> Os códigos estão no arquivo **04-incluindo-editando-e-removendo-colunas-de-tabelas.sql**


## [Aula 399] - Filtrando registro (WHERE)
[Link aula](https://www.udemy.com/course/web-completo/learn/lecture/11144716#overview)<br><br>

**Para que serve um filtro?**
Para buscar um determinado valor com uma determinada condição de busca.
<br><br>

**Exemplo cláusula WHERE**<br>
SELECT id_curso, nome_curso FROM cursos WHERE investimento < 500<br>
SELECT id_curso, nome_curso FROM cursos WHERE investimento < 500 AND carga_horaria > 15
<br><br>

**Operadores de comparação:**
- **=** | Valor da esquerda **igual** ao valor da direita
- **<** | Valor da esquerda **menor** que o valor da direita
- **<=** | Valor da esquerda **menor ou igual** ao valor da direita 
- **>** | Valor da esquerda **maior** que o valor da direita
- **>=** | Valor da esquerda **maior ou igual** ao valor da direita

**Operadores lógicos:**
- **AND** | Todos as operações de comparação devem ser verdadeiros
- **OR** | Pelo meno uma das operações de comparação deve ser verdadeira


## [Aula 414] - Introdução ao relacionamento entre tabelas, chave primária e estrangeria
[Link aula](https://www.udemy.com/course/web-completo/learn/lecture/11144916#overview)<br><br>

**Tipos de relacionamentos**
 - Um para UM
 - Um para Muitos
 - Muitos para Muitos

**Chave primária**<br>
**Chave estrangeira**<br>

**Exemplo: [Um para Muitos]**
[tb_alunos] > [tb_cursos]<br>
[tb_cursos] > [tb_disciplinas]
<br><br>

**Exemplo: [Um para Muitos]**
[tb_alunos] > [tb_cursos]<br>
[tb_cursos] > [tb_disciplinas]
<br><br>






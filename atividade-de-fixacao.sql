﻿/*1) Selecione todos os clientes com idade igual ou superior a 29. Os registros devem ser ordenados de forma ascendente pela idade*/
SELECT
  *
FROM
  tb_clientes;

SELECT
  *
FROM
  tb_clientes
WHERE
  idade > 29
ORDER BY idade
  ASC;


  /*2) Utilize instruções do subconjunto DDL do SQL para realizar a inclusão das colunas abaixo na tabela tb_clientes:
  
  Adicine a coluna “sexo” do tipo string com tamanho fixo de 1 caractere. Coluna não pode ser vazia na inserção.
  
  Adicione a coluna “endereço” do tipo string com tamanho variado de até 150 caracteres. Coluna pode ser vazia na inserção.*/

ALTER TABLE tb_clientes ADD COLUMN sexo CHAR(1) NOT NULL;
-- ALTER TABLE tb_clientes CHANGE sexo sexo CHAR(1) NOT NULL;

ALTER TABLE tb_clientes ADD COLUMN endereco VARCHAR(150) NULL;


  /*3) Efetue um update em tb_clientes dos registros de id_cliente igual a 1, 2, 3, 6 e 7, atualizando o sexo para “M”. Utilize a instrução IN para este fim.*/
  UPDATE tb_clientes SET sexo = 'M' WHERE id_cliente IN(1,2,3,4,5,6,7);


  /*4) Efetue um update em tb_clientes dos registros de id_cliente igual a 4, 5, 8, 9 e 10, atualizando o sexo para “F”. Como desafio, faça este update utilizando dois between’s no filtro.*/

  UPDATE tb_clientes SET sexo = 'F' WHERE (id_cliente BETWEEN 4 AND 5) OR (id_cliente BETWEEN 8 AND 10);

  SELECT * FROM tb_clientes;

  /*5) Selecione todos os registros de tb_clientes que possuam relação com tb_pedidos e com tb_pedidos produtos (apenas registros com relacionamentos entre si). Recupe também os detalhes dos produtos da tabela tb_produtos. A consulta deve retornar de tb_clientes as colunas “id_cliente”, “nome”, “idade” e de tb_produtos deve ser retornado as colunas “produto” e “valor”.*/


  SELECT 
    C.id_cliente, C.nome, C.idade, PRO.produto, PRO.valor 
FROM 
    tb_clientes AS C
    INNER JOIN tb_pedidos AS PE ON C.id_cliente = PE.id_cliente
    INNER JOIN tb_pedidos_produtos AS PP ON(PE.id_pedido = PP.id_pedido)
    LEFT JOIN tb_produtos AS PRO ON PP.id_produto = PRO.id_produto;

  
  SELECT * FROM tb_produtos;
  ------------------------------------------------------
  -- INSERINDO ALGUNS CLIENTES
  -- INSERT INTO tb_clientes (nome,idade) VALUES ("Amos",19),("Porter",22),("Abbot",47),("Chancellor",50),("Lacey",47),("Kato",44),("Mira",20),("Daphne",24),("Lisandra",61),("Chaim",72);
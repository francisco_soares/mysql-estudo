﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;


/* ========== // ========== */

/*[INSERT INTO <nome-tabela>(colunas) VALUES(valores)]
Comando para inserir dados em uma tabela do banco de dados
*/
INSERT INTO cursos(id_curso, imagem_curso, nome_curso, resumo, data_cadastro, ativo, investimento, carga_horaria) 
VALUES('', 'curso-php.jpg', 'Curso de PHP', 'Curso de formação completa de PHP', '2021-08-29 16:38:30', 1, 359.50, '20');


-- ALTER TABLE cursos CHANGE id_curso id_curso INT NOT NULL PRIMARY KEY AUTO_INCREMENT;

SELECT * FROM cursos;





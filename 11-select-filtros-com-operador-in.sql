﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;
/* ========== // ========== */
/*(AULA: 403)*/
/*Usando filtro IN*/
-- O IN é utilizado como filtro de possibilidades, onde eu posso informar os conteúdos que deve ser retornado no select.
SELECT
  *
FROM
  alunos
WHERE
  interesse IN('Jogos', 'Música', 'Esporte');
-- Como fica o IN com o OR
SELECT
  *
FROM
  alunos
WHERE
  interesse = 'Jogos'
  OR interesse = 'Musica'
  OR interesse = 'Esporte';


-----------------------------------
  -- [NOT IN] - No exemplo abaixo ele vai descartar as três possibilidades (Jogos, Musicas e Esportes)
SELECT
  *
FROM
  alunos
WHERE
  interesse NOT IN('Jogos', 'Música', 'Esporte');
﻿/*(AULA: 418)*/
/*CRIANDO RELACIONAMENTO MUITOS PARA MUITOS*/
-- EXPLICAÇÃO:
-- tb_produtos: Um produto pode ter  vários/MUITOS(AS) Imagens tb_imagens.

/* ========================================= */
/* ===== CRIANDO A TABELA: tb_clientes ===== */
/* ========================================= */
CREATE TABLE tb_clientes(
    id_cliente INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(100) NOT NULL,
    idade INT(3) NOT NULL
);

DESCRIBE tb_clientes;


/* ======================================== */
/* ===== CRIANDO A TABELA: tb_pedidos ===== */
/* ======================================== */
CREATE TABLE tb_pedidos(
    id_pedido INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    id_cliente INT NOT NULL,
    data_hora DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY(id_cliente) REFERENCES tb_clientes(id_cliente)
);

DESCRIBE tb_pedidos;

/* ================================================= */
/* ===== CRIANDO A TABELA: tb_pedidos_produtos ===== */
/* ================================================= */
CREATE TABLE tb_pedidos_produtos(
    id_pedido INT NOT NULL,
    id_produto INT NOT NULL,
    FOREIGN KEY(id_pedido) REFERENCES tb_pedidos(id_pedido),
    FOREIGN KEY(id_produto) REFERENCES tb_produtos(id_produto)
);

DESCRIBE tb_pedidos_produtos;


/* ========================================== */
/* ===== [AULA 419] - Populando tabelas ===== */
/* ========================================== */

-- Usando o método da loja virtual

-- CADASTRO DE USUÁRIO
INSERT INTO tb_clientes(nome, idade) VALUES("Francisco", 32);
SELECT * FROM tb_clientes;

/*  ===== USUÁRIO FAZ UM PEDIDO ===== */
INSERT INTO tb_pedidos(id_cliente) VALUES(1);

/*  ===== PEDIDO DO PRODUTO  ===== */
INSERT INTO tb_pedidos_produtos(id_pedido, id_produto) VALUES(1, 2);
INSERT INTO tb_pedidos_produtos(id_pedido, id_produto) VALUES(1, 3);
SELECT * FROM tb_pedidos_produtos;

/*  ===== CLIENTE FAZ UM NOVO PEDIDO  ===== */
INSERT INTO tb_pedidos(id_cliente) VALUES(1);
SELECT * FROM tb_pedidos;

/* ===== NOVO PEDIDO DO PRODUTO =====*/
INSERT INTO tb_pedidos_produtos(id_pedido, id_produto) VALUES(2, 3);
SELECT * FROM tb_pedidos_produtos;

/* ===== NOVO CLIENTE ===== */
INSERT INTO tb_clientes(nome, idade) VALUES("Isa", 25);
SELECT * FROM tb_clientes;
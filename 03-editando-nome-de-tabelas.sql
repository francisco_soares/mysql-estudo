﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;


/* ========== // ========== */
/*[RENAME TABLE <nome-atual-da-tabela> TO <novo-nome>]
Comando para alterar o nome de uma tabela
*/
RENAME TABLE cursos_teste TO cursos;
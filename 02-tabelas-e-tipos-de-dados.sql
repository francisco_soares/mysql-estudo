﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
use mysql_estudo;

DROP TABLE cursos;

/*[CREATE TABLE] - Comando para criar uma tabela*/
CREATE TABLE cursos(
    id_curso int not null, -- Criando um ID como INTEIRO, Ñ aceita nulo
    imagem_curso varchar(100) not null,
    nome_curso char(50) not null,
    resumo text null, -- NULL pode ser nulo ou não
    data_cadastro datetime not null,
    ativo boolean default true, -- Possui um valor, ex: TRUE, verdadeiro
    investimento float(8, 2) default 0 -- 75344 vira 753.44
);

/*[DROP TABLE <NOME DA TABELA>] - Comando para deletar a tabela*/
/*Lembrando que o bando deve estar selecionado*/
DROP TABLE cursos;
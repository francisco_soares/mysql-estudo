# Utilizando o PHPMyAdmin para manipulação do MySQL.

![Utilizando o PHPMyAdmin para manipulação do MySQL.](img/img-1.png)

## O que é PHPMyAdmin?
O **PHPMyAdmin** é uma aplicação web construida em **PHP** que nos ajuda a manipular e administrar o MySQL via interface.<br><br>

O **PHPMyAdmin** vem por padrão em algumas aplicações para ambiente local como o **XAMPP**, **WAMP** e **LAMP**(para Linux).<br>
Em hospedagens web também é implementado o **PHPMyAdmin**.<br><br>

### Como acessar?
Partindo do princípio que o **XAMPP**, **WAMP** e **LAMP** esteja instalado em sua máquina, você pode acessar via url **localhost/phpmyadmin/** ou **127.0.0.1/phpmyadmin**
﻿/*[use banco-de-dados] - Comando para acessar o banco de dados*/
/*Lembre sempre de antes de usar qualquer comando, usar esse abaixo*/
use mysql_estudo;
/* ========== // ========== */
/*(AULA: 412)*/
/*DELET*/
-- Deletando registro dentro das tabelas no banco de dados
SELECT * FROM alunos;

DELETE FROM alunos WHERE id_aluno = 5;

SELECT * FROM alunos WHERE idade IN(10,18,22,28,34) AND interesse = 'Esporte';


DELETE FROM alunos WHERE idade IN(10,18,22,28,34) AND interesse = 'Esportes'